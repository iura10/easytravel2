# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from events.models import Event

class CrawlerPipeline(object):

	def process_item(self, item, spider):

		Event_title = item["event_title"]

		if Event.objects.filter(event_title=Event_title).exists():
			return item["event_title"] + "\n !!! Event already exists in Database !!!\n"
		else:
			item.save()
			return item

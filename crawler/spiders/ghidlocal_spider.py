# -*- coding: utf-8 -*-
import scrapy
from crawler.items import Event
import urllib.parse

class GhidlocalSpider(scrapy.Spider):
	name = 'ghidlocal_spider'
	allowed_domains = ['ghidlocal.com']
	start_urls = ['https://ghidlocal.com/oradea/evenimente']

	def parse(self, response):
		for element in response.css("div.type-tribe_events"):
			event = Event()
			# event.set_all(None)

			event['event_title'] = element.css("a.tribe-event-url::text").extract_first().strip()
			event['event_preview'] = element.css("[itemprop='description']>p::text").extract_first()
			event['event_start_time'] = element.css("[itemprop='startDate']::attr(content)").extract_first()
			event['event_end_time'] = element.css("[itemprop='endDate']::attr(content)").extract_first()

			event['event_location_text'] = element.css("span.tribe-street-address::text").extract_first() #location text

			classes = element.css("div.type-tribe_events::attr(class)").extract_first() #type
			if "category" in classes:
				event['event_type'] = [x for x in classes.split() if "category" in x][0][22:]

			url = element.css("a.tribe-events-read-more::attr(href)").extract_first() #description (text)
			event['event_URL'] = url
			yield scrapy.Request(url, meta={"event": event}, callback=self.parse_details)

		next_page = response.css('li.tribe-events-nav-next > a::attr(href)').get()
		if next_page is not None:
			yield response.follow(next_page, callback=self.parse)


	def parse_details(self, response):
		event = response.meta["event"]
		event['event_text'] = "\n".join(response.css("div.tribe-events-single-event-description > p::text").extract())
		url = response.css("div.tribe-events-event-image > img::attr(src)").extract_first()
		if url:
			event['event_image_url'] = url
		yield event